package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	public MyLeadsPage clickLeadsButton() {
		
	
		WebElement leads = locateElement("xpath","//a[text()='Leads']");
		click(leads);
		return new MyLeadsPage();
}
}