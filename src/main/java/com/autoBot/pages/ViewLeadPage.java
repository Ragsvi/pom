package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{
	public ViewLeadPage verifyFirstName(String excelFirstName) {
WebElement firstNm = locateElement("id","viewLead_firstName_sp");
getElementText(firstNm);

verifyExactText(firstNm,excelFirstName);
return this;
		
}
}