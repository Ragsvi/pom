package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class CreateLeadPage extends Annotations {
	public CreateLeadPage enterCompanyName(String cname) {
		WebElement compName = locateElement("createLeadForm_companyName");
		clearAndType(compName, cname); 
		
		return this;
	}
	
	public CreateLeadPage enterFirstName(String firstName) {
		WebElement fname = locateElement("id","createLeadForm_firstName");
		clearAndType(fname, firstName);
		return this;
	}
	public CreateLeadPage enterLastName(String lname) {
		WebElement lastName = locateElement("id","createLeadForm_lastName");
		clearAndType(lastName,lname);
		return this;
	}
	public ViewLeadPage clickOnCreateLeadButton() {
		WebElement lastName = locateElement("xpath","//input[@value='Create Lead']");
		click(lastName);
		return new ViewLeadPage();
	}
}
