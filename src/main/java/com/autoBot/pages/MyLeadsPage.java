package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class MyLeadsPage extends Annotations{
	public CreateLeadPage clickCreateLeadButton() {
		WebElement createLeadButton = locateElement("xpath","//a[text()='Create Lead']");
		click(createLeadButton);
		return new CreateLeadPage();

	}
}